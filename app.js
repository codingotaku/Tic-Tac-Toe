const board =[
    ['','',''],
    ['','',''],
    ['','','']
];

let available = 9;

const players = ['X', 'O'];
let currentPlayer;
let areaW;
let areaH;
let winner = '';

function setup(){
    const padding = 30;
    createCanvas(windowWidth-padding,windowHeight-padding);
    areaW = windowWidth > windowHeight? windowHeight-padding : windowWidth-padding;
    areaH=areaW;
    currentPlayer = 'X';
}

function checkWinner(){
    for(let i=0; i <3; i++){
        console.log(board[i][0] === board[i][1] && board[i][1] === board[i][2]);
        if(board[i][0] === board[i][1] && board[i][1] === board[i][2]){
            winner = board[i][0];
            if(winner) break;
        }
        if(board[0][i] === board[1][i] && board[1][i] === board[2][i]){
            winner = board[0][i];
            if(winner) break;
        }
    }
    if(!winner){
        if(board[0][0] === board[1][1] && board[1][1] === board[2][2]){
            winner = board[0][0];
        }
        if(board[2][0] === board[1][1] && board[1][1] === board[0][2]){
            winner = board[0][2];
        }
    }

    if( !winner && !available){
        winner = "Tie"
    }
}

function resetGame(){
    if(!winner) return;
    winner = '';
    for(let i=0;i<3;i++){
        for(let j=0;j<3;j++){
            board[i][j] = '';
        }
    }
    currentPlayer = 'X';
    available = 9;
}

function mouseClicked(){
    if(winner){
        resetGame();
        return;
    }

    let x = floor(mouseY / (areaH / 3));
    let y = floor(mouseX / (areaW / 3));

    if(x <= 3 && y<=3 && !board[x][y]){
        board[x][y] =  currentPlayer;
        available--;
        currentPlayer = currentPlayer == players[0]? players[1]: players[0];
        checkWinner();
    }
}

function draw(){
    background(52);
    const w = areaW*0.3;
    const h = areaH*0.3;

    noFill()
    stroke(200);
    strokeWeight(4);
    line(0, 0, 0, h*3);
    line(0, 0, w*3, 0);
    line(w*3, 0, w*3, h*3);
    line(0, h*3, w*3, h*3);

    line(w * 0.3, h, w * board.length - w * 0.3, h);
    line(w * 0.3, h * 2, w * board.length - w * 0.3, h*2);

    line(w, h * 0.3, w, h * board.length - h * 0.3);
    line(w * 2, h * 0.3, w * 2, h * board.length - h * 0.3);


    for (let i = 0; i < 3; i++) {
        for (let j = 0; j< 3; j++) {
            let x = w * i + w * 0.5;
            let y = h * j + h * 0.5;
            let item = board[j][i];

            fill(255)
            stroke(255);
            textAlign(CENTER, CENTER);
            textSize(w*0.5);
            text(item, x, y);
        }
    }
    if(winner){
        fill(52,150);
        noStroke();
        rect(0,0, areaW, areaH);
        fill(32,200);
        rect(w*0.25,h*0.1, areaW - w * 0.5, h*2.3);

        fill(255);
        textSize(w*0.2);
        if(winner === "Tie"){
            text(`It's a Tie!`, areaW*0.5 , h*0.5);
        }else{
            text(`Player ${winner} wins!`, areaW*0.5 , h*0.5);
        }
        text(`Click anywhere to restart`, areaW*0.5 , h*2);
    }
}
